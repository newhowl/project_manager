import 'package:common_util/util/map_util.dart';
import 'package:project_manager/platform/platform_constant.dart';
import 'package:project_manager/platform/platform_enum.dart';
import 'package:project_manager/platform/platform_model.dart';

class PlatformInventory {
  static final _instance = PlatformInventory._internal();

  PlatformInventory._internal();

  factory PlatformInventory() {
    return _instance;
  }

  ///
  ///
  ///
  Map<PlatformType, PlatformKind> _inventory = {
    PlatformType.android: PlatformKind.origin(
      code: PLATFORM_ANDROID_ID,
      name: PLATFORM_ANDROID,
      platformKindType: PlatformType.android,
      storeUrl: '',
    ),
    PlatformType.ios: PlatformKind.origin(
      code: PLATFORM_IOS_ID,
      name: PLATFORM_IOS,
      platformKindType: PlatformType.ios,
      storeUrl: '',
    ), 
  };

  ///
  /// 
  /// 
  Map<PlatformType, PlatformKind> get inventory => _inventory;

  PlatformKind? inventoryByType(PlatformType platformType) {
    return getItemFromMap(inventory, platformType);
  }
}
