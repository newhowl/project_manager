import 'package:project_manager/platform/platform_enum.dart';

class PlatformKind {
  int code = 0;

  String name = '';

  PlatformType platformKindType = PlatformType.android;

  String storeUrl = '';

  PlatformKind.origin({
    this.code = 0,
    this.name = '',
    this.platformKindType = PlatformType.android,
    this.storeUrl = '',
  });

  @override
  String toString() {
    return 'toString > PlatformKind > '
        'code : $code '
        'name : $name '
        'platformKindType : $platformKindType '
        'storeUrl : $storeUrl '
    ;
  }
}